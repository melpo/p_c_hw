import argparse
import os
import time
parser = argparse.ArgumentParser()
parser.add_argument("dirpath", type=str,
                    help="Enter the way and folder name")
parser.add_argument("-d", "--day", action='store_true',
                    help="add day in name")
parser.add_argument("-m", "--month", action='store_true',
                    help="add month in name")
parser.add_argument("-y", "--year", action='store_true',
                    help="add year in name")
args = parser.parse_args()
def create_directory(dirpath,d=False, m=False, y=False):

    answer = str(dirpath)
    if d:
        answer = answer + "-" + str(time.strftime("%d",time.localtime()))
    if m:
        answer = answer + "-" +  str(time.strftime("%m", time.localtime()))
    if y:
        answer = answer + "-" +  str(time.strftime("%Y", time.localtime()))
    if d==False and m==False and y==False:
        answer = str(args.dirpath) + '/unknow'
    try:
        os.makedirs(answer)
    except FileExistsError:
        print("folder " +str(answer) + " already exist")
    else: print(answer)
create_directory(args.dirpath, d= args.day, m=args.month, y=args.year)
