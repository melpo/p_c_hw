from decimal import Decimal
class Money:
    def __init__(self, money1, money2=0, course=0): # The class contains 3 arguments
        self.money1 = money1
        self.money2 = money2
        self.course = course
    def showMoney(self):
        money1_d = Decimal(self.money1)
        a = int(money1_d)
        b = int(100 * (money1_d - a))
        return str(a) +"," + str(b)
    def getMinus(self):
        money = Decimal(self.money1) - Decimal(self.money2)
        a = int(money)
        b = int(100 * (money - a))
        return str(a) +"," + str(b)
    def getPlus(self):
        money = Decimal(self.money1) + Decimal(self.money2)
        a = int(money)
        b = int(100 * (money - a))
        return str(a) +"," + str(b)
    def getSeparate(self):
        money = Decimal(self.money1 / self.money2)
        a = int(money)
        b = int(100 * (money - a))
        return str(a) +"," + str(b)
    def getRelation(self):
        money_1 = Decimal(self.money1)
        a = int(money_1)
        b = int(100 * (money_1 - a))
        money_2 = Decimal(self.money2)
        c = int(money_2)
        d = int(100 * (money_2 - c))
        if self.money1==self.money2:
            return str(a) +"," + str(b) + ' and ' + str(c) +"," + str(d) +" it is the same"
        elif self.money1>self.money2:
            return str(a) + "," + str(b) + ' more than ' + str(c) + "," + str(d)
        else:
            return str(a) + "," + str(b) + ' more than ' + str(c) + "," + str(d)
    def setCourse(self, course):
        self.course = course
    def getCourse(self):
        money_1 = Decimal(self.money1*self.course)
        a = int(money_1)
        b = int(100 * (money_1 - a))
        return str(a) + "," + str(b)
m = Money(1000.50)

n = Money(100.1, 100.2)
print(m.showMoney())
print(n.getMinus())
print(n.getPlus())
print(n.getSeparate())
print(n.getRelation())
m.setCourse(23)
print(m.getCourse())
