import argparse
import os
import time

parser = argparse.ArgumentParser()
parser.add_argument("dirpath", type=str,
                    help="Enter the way and folder name")
parser.add_argument("-d", "--day", action='store_true',
                    help="add day in name")
parser.add_argument("-m", "--month", action='store_true',
                    help="add month in name")
parser.add_argument("-y", "--year", action='store_true',
                    help="add year in name")
args = parser.parse_args()

def create_directory(dirpath,d=0, m=0, y=0):

    answer = str(dirpath) #+str(time.strftime(d,time.localtime()))
    if d!=0:
        answer = answer + "-" + str(time.strftime(d,time.localtime()))
    if m!=0:
        answer = answer + "-" +  str(time.strftime(m, time.localtime()))
    if y!=0:
        answer = answer + "-" +  str(time.strftime(y, time.localtime()))
    try:
        os.makedirs(answer)
    except FileExistsError:
        print("folder " +str(answer) + " already exist")
    else: print(answer)
if args.day and args.month and args.year:
    create_directory(args.dirpath,d="%d", m="%m", y="Y")
elif args.day and args.month:
    create_directory(args.dirpath, d="%d", m="%m")
elif args.day and args.year:
    create_directory(args.dirpath, d="%d", m="%Y")
elif args.month and args.year:
    create_directory(args.dirpath, d="%m", m="%Y")
elif args.day:
    create_directory(args.dirpath, d="%d")
elif args.month:
    create_directory(args.dirpath, m="%m")
elif args.year:
    create_directory(args.dirpath, m="%Y")

else:
    answer = str(args.dirpath) + '/unknow'
    try:
        os.makedirs(answer)
    except FileExistsError:
        print("folder " +str(answer) + " already exist")
    else: print(answer)
