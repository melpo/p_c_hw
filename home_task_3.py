from time import time as tme

def my_decorator(calculate_words):
    def wrapper(way):
        t_start = tme()
        calculate_words(way)
        t_fin = tme()
        print(float(t_fin-t_start))
    return wrapper
@my_decorator
def calculate_words(way):

    with open(way, "r") as file:
        line = file.readline().split()
        word=1
        number_words = 0
        for i in line:
            number_words+=word
        print(number_words)

file_name = 'for_read_hw3.txt'
calculate_words(file_name)

