class User:
    def __init__(self, name, age):
        self.name = name
        self.age = age
    def setName(self,name):
        self.name = name
    def getName(self):
        return self.name
    def setAge(self,age):
        self.age = age
    def getAge(self):
        return self.age
class Worker(User):
    def __init__(self, name, age, salary):
        super().__init__(name, age)
        self.salary = salary
    def setSalary(self,salary):
        self.salary = salary
    def getSalary(self):
        return self.salary
i = Worker('Ivan',25,1000)
v = Worker('Vasja',26,2000)
print (v.getSalary() - i.getSalary())
class Student(User):
    def __init__(self, name, age, course, grant):
        super().__init__(name, age)
        self.course = course
        self.grant = grant
    def setCourse(self, course):
        self.course = course
    def getCourse(self):
        return self.course
    def setGrant(self, grant):
        self.grant=grant
    def getGrant(self):
        return self.grant
class Driver(Worker):
    def __init__(self, name,age,salary,work_age,category):
        super().__init__(name, age, salary)
        self.work_age = work_age
        self.category = category
    def setWorkAge(self, work_age):
        self.work_age=work_age
    def getWorkAge(self):
        return self.work_age
    def setCaregory(self, category):
        self.category=category
    def getCategory(self):
        return self.category

m = Driver('Lexa', 37, 20000, 17, 'A')
print(m.getCategory())

